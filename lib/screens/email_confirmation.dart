import 'package:amplify_core/amplify_core.dart';
import 'package:flutter/material.dart';

import 'main_screen.dart';

class EmailConfirmationScreen extends StatelessWidget {
  final String email;

  EmailConfirmationScreen({super.key, required this.email});

  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final TextEditingController _confirmationCodeController =
      TextEditingController();
  final _formKey = GlobalKey<FormFieldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: const Text("Confirm your email"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              Text(
                "An email confirmation code is sent to $email. Please type the code to confirm your email.",
              ),
              TextFormField(
                keyboardType: TextInputType.number,
                controller: _confirmationCodeController,
                decoration:
                    const InputDecoration(labelText: "Confirmation Code"),
                validator: (value) => value!.length != 6
                    ? "The confirmation code is invalid"
                    : null,
              ),
              OutlinedButton(
                onPressed: () => _submitCode(context),
                child: const Text("CONFIRM"),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _submitCode(BuildContext context) async {
    final confirmationCode = _confirmationCodeController.text;
    try {
      var signUpResult = await Amplify.Auth.confirmSignUp(
        username: email,
        confirmationCode: confirmationCode,
      );
      if (signUpResult.isSignUpComplete) {
        _gotoMainScreen(context);
      }
    } catch (e) {
      safePrint("Confirm Error: $e");
    }
  }

  void _gotoMainScreen(BuildContext context) {
    Navigator.push(context, MaterialPageRoute(builder: (_) => MainScreen()));
  }
}
